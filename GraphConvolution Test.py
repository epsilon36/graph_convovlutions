from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import tensorflow as tf
from tensorflow_graphics.notebooks import mesh_segmentation_dataio as dataio

from tensorflow_graphics.nn.layer import graph_convolution as graph_conv
import json
import numpy as np

TrainList = r"C:\Users\Arren Bustamante\Documents\OBJ Snapshots\Projection Profiling\System 2\TrainList.txt"
ModelPath = r"C:\Users\Arren Bustamante\Documents\OBJ Snapshots\Projection Profiling\System 2\Model Directory\Model2"
dataFolder = r'C:\Users\Arren Bustamante\Documents\Data'


Meshes = []
LandMarks = []
months = os.listdir(dataFolder)
#Iterate through all months in the folder.
Objects = []

def GenerateMesh(meshFile,landMarkFile):
    
    ######################Read Mesh File###########################################
    with open(meshFile,'r') as f:
        lines = f.readlines()
           
    _Vertices = []
#    Vertices = []
    Triangles = []
    Upper = []
    for line in lines:
        if line[0] == 'v':
            vertex = line.split()[1:4]
            _Vertices.append(vertex)
        if line[0] == 'f':
            triangle = line.split()[1:4]
            Triangles.append(triangle)
    Triangles = np.array(Triangles,dtype=np.int32)-1
    
    Vertices = _Vertices
    Vertices = np.array(Vertices,dtype=np.float32)
    Triangles = np.array(Triangles,dtype=np.int32)
    
    ##################Read Landmark file####################################################

    Upper = Vertices[0]
    
####################Obtain Distances#######################################    
    Labels = np.zeros(len(Vertices),dtype=np.int64)
    Labels[0] = 1
    Output = {'Vertices':Vertices,'Triangles':Triangles,'Labels':Labels}
    return Output,Vertices,Triangles,Labels

Vertices = []
Triangles = []
Labels = []


def ZeroPad(myList,mode='TV'):
    OutList = []
    Lengths = []
    maxLength = 0
    for i in range(len(myList)):
        length = myList[i].shape[0]
        Lengths.append(length)
        if length > maxLength:
            maxLength = length
    for i in range(len(myList)):
        z = maxLength-myList[i].shape[0]
        if mode == 'TV':
            outList = np.append(myList[i],np.zeros((z,3)),axis=0)
        elif mode == 'L':
            outList = np.append(myList[i],np.zeros((z)),axis=0)
        OutList.append(outList)
    return np.array(OutList),np.array(Lengths)



#################################
#upper = r"C:\Users\Arren Bustamante\Documents\OBJ Snapshots\Projection Profiling\TestTetraHedron.obj"
#upper = r"C:\Users\Arren Bustamante\Documents\OBJ Snapshots\Projection Profiling\IsoHedron.obj"
upper = r"C:\Users\Arren Bustamante\Documents\OBJ Snapshots\Projection Profiling\SpikeTetraHedron.obj"
landmark = r"C:\Users\Arren Bustamante\Documents\Data\2019.02\200242\landmark.lnd"
Outputs,vertices,triangles,labels = GenerateMesh(upper,landmark)

N_meshes = 10
Triangles = np.array([triangles]*N_meshes)
_Vertices = np.array([vertices]*N_meshes)+np.random.normal(0,.1,(vertices.shape))
Labels = np.array([labels]*N_meshes)
#############################################
import quaternion
def Rotate(array,Q):
    if array.ndim == 2:
        _P = np.concatenate((np.zeros((len(array),1)),array),1)
    else:
        _P = np.concatenate((np.array([0]),array),axis=0)
    P = quaternion.as_quat_array(_P)
    Qi = Q.conj()
    result =  np.dot(np.dot(Q,P),Qi)
    if array.ndim == 2:
        return quaternion.as_float_array(result)[:,1:4]
    else:
        return quaternion.as_float_array(result)[1:4]
    
Vertices = []
for i in range(N_meshes):
    Q = quaternion.from_float_array(np.random.rand(4))
    mesh = Rotate(_Vertices[i],Q)
    Vertices.append(mesh)

Vertices = np.array(Vertices)

Vertices,VertLength = ZeroPad(Vertices)
Triangles,TriLength = ZeroPad(Triangles)



import sparse

def SPDiag(dia):
    """Creates a sparse diagonal matrix given a diagonal, a tensor n dimension, and 
    the size of the square"""
    n,s = dia.shape
    
    ns = DepthVector(n,s,np.float32)
    _x = np.tile(np.array(range(s)),(n))
    x = np.expand_dims(_x,axis=1)
    y = x

    I = np.concatenate((ns,x,y),axis=1)
    values = dia.reshape(-1)
    Sparse = sparse.COO(I.T,data=values,shape=(n,s,s))
    
    valuesInv2 = np.reshape(.5/values,(n,s))
    return Sparse,valuesInv2

    
def DepthVector(n,xy,a):
    """Calculates the Q array which creates the n dimension in a sparse array"""
    #We now need to take the first dimension of the original _indeces and use it as the zero axis dimension
        #The value of this dimension is the index of the mesh model.
    #Instantiate with Zeros.  The first t elements correspond to the first mesh
    Q = np.zeros(xy,dtype=a)
    #The next t elements correspond to the second mesh and so forth.
        #The index of i ends up being the coordinate and that coordinate is repeated t times.  
        #We concatenate this throughout the loop so that we end up with a vector that is n*t long.
        #Since the zero element is already instantiated, we go to the range of t-1
    for i in range(n-1):
        Q = np.concatenate((Q,np.ones(xy,dtype=a)+i))
    Q = np.expand_dims(Q,axis=1)
    return Q

def MakeAdj(triangles):
    #Ensure that triangles are in int form
    triangles = triangles.astype(np.int32)
    #Individual points of each triangle
    a = np.expand_dims(triangles[:,:,0],2)
    b = np.expand_dims(triangles[:,:,1],2)
    c = np.expand_dims(triangles[:,:,2],2)
    
    #Define every edge (in both directions)
    A = np.concatenate((a,b),axis=2)
    B = np.concatenate((a,c),axis=2)
    C = np.concatenate((b,c),axis=2)
    D = np.flip(A,axis=2)
    E = np.flip(B,axis=2)
    F = np.flip(C,axis=2)
    
    #Combine all edges
    _indices = np.concatenate((A,B,C,D,E,F),axis=1)
    #Flatten the array such that it takes the form [xy0,xy0,xy0,xy1,xy1,xy1]
    indices = _indices.reshape((-1,2))
    
    Q = DepthVector(_indices.shape[0],_indices.shape[1],np.int32)

    #Concatenate this vector to the indeces array
    Indices = np.concatenate((Q,indices),axis=1)
    Values = np.ones(Indices.shape[0])
    
    Sparse = sparse.COO(Indices.T,Values)
#    maxIndex = np.max(_indices[:,:,0],axis=1)
    
    d = sparse.COO.sum(Sparse,axis=2).todense()
    D,dI2 = SPDiag(d)
    Sparse = Sparse+D
    Adj = Sparse*np.expand_dims(dI2,2)
    
    return Adj
############################Mesh Encoder###########################################
Adj = MakeAdj(Triangles) 
sz = np.array([10,11,12,9,8,10,12,11,12,12])
#sz = np.array([12,12,12,12,12,12,12,12,12,12])
#######################################Graph Start########################################################
v = Vertices.shape[1]

V0 = tf.placeholder(dtype=tf.float64,name="V0",shape=[10,v,3])
adj_sp = tf.sparse_placeholder(shape=[None,v,v],dtype=tf.float64,name='SP')
L = tf.placeholder(dtype=tf.float64,name="labels",shape=[10,v])

#V0 = tf.constant(Vertices,dtype=tf.float64)
#adj_sp = tf.SparseTensor(indices=Adj.coords.T,values=Adj.data,dense_shape=np.array(Adj.shape,dtype=np.int64))
#L = tf.constant(Labels,dtype=tf.float64)

sizes = tf.constant(sz,dtype=tf.int64)
#sizes = None
#############################Redone Library###############################################
Data = V0 ##Switch Over
C = int(Data.shape[-1])
D = 1
W = 1

config = tf.ConfigProto(allow_soft_placement=True)
config.gpu_options.allow_growth = True

with tf.device('/cpu:0'):

    ####Variables
    var_u = tf.Variable(tf.truncated_normal((C,W),dtype=tf.float64))
    var_v = tf.Variable(tf.truncated_normal((C,W),dtype=tf.float64))
    var_c = tf.Variable(tf.truncated_normal((W,),dtype=tf.float64))
    var_w = tf.Variable(tf.truncated_normal((W,C,D),dtype=tf.float64))
    var_b = tf.Variable(tf.truncated_normal((D,),dtype=tf.float64))
    
    ####Flatten Data
################################## 
    if sizes != None:
#    if False:
        sizes_square = sizes
        mask = tf.sequence_mask(sizes_square, Data.shape[-2])
        mask_indices = tf.cast(tf.where(mask), tf.int64)
        F = tf.reduce_sum(sizes)
        MI = tf.reshape(mask_indices,(F,2))
        x_flat = tf.gather_nd(params=Data, indices=MI)
        
##################################################    
    else:
        x_flat = tf.reshape(Data,(-1,V0.shape[-1]))

    #outShape = np.array(Data.shape)
    outShape = tf.constant([Data.shape[0],Data.shape[1],D],dtype=tf.int64)
    sparseShape = tf.cast(tf.shape(adj_sp),tf.int64)
    ########Flatten Adjacency matrix
    sparseAdj = tf.sparse.reshape(adj_sp,[-1,sparseShape[-2],sparseShape[-1]])
    indices = sparseAdj.indices
    
    
    ###Flatten Adj
###########################
    if sizes != None:
#    if False:
        sizeSquare = tf.stack((sizes,sizes),axis=-1)
        cumsum = tf.cumsum(sizeSquare, axis=0, exclusive=True)
        index_shift = tf.gather(cumsum, indices[:, 0])
        indices = indices[:, 1:] + index_shift
        flatAdj = tf.SparseTensor(indices, sparseAdj.values,tf.reduce_sum(input_tensor=sizeSquare, axis=0))    

############################    
    else:
        index_shift = tf.multiply(tf.expand_dims(indices[:,0],-1),sparseShape[1:])
        flatIndices = tf.add(indices[:,1:],index_shift)
        flatAdj = tf.SparseTensor(flatIndices,sparseAdj.values,sparseShape[0]*sparseShape[1:])
##############################
    #Multiply flattened data values by linear variables
    x_u = tf.matmul(x_flat,var_u)
    x_v = tf.matmul(x_flat,var_v)
    
    #Get sparse adjacency matrix indices for Mx and My
    flatAdj_0 = flatAdj.indices[:,0]
    flatAdj_1 = flatAdj.indices[:,1]
    
    #Gather the appropriate data values corresponding to the adjacency matrix
    x_u_rep = tf.gather(x_u,flatAdj_0)
    x_v_sep = tf.gather(x_v,flatAdj_1)
    
    #soft assign
    weights_q = tf.exp(tf.add(tf.add(x_u_rep,x_v_sep),tf.reshape(var_c,(1,-1))))
    weights_q_sum = tf.reduce_sum(weights_q,axis=-1,keepdims=True)
    crossE = tf.divide(weights_q,weights_q_sum)
    
    #
    x_sep = tf.gather(x_flat,flatAdj_1)
    

    
######################
    
    QList = tf.unstack(crossE,axis=-1)
    WList = tf.unstack(var_w,axis=0)
    def Partition_sum(data,group_ids,row_weights):
        num_rows = tf.size(input=group_ids, out_type=tf.int64)
        sparse_indices = tf.stack((group_ids, tf.range(num_rows)), axis=1)
        out_shape = (tf.reduce_max(input_tensor=group_ids) + 1, num_rows)
        sparse = tf.SparseTensor(sparse_indices, row_weights, dense_shape=out_shape)
        return tf.sparse.sparse_dense_matmul(sparse, data)
    
    y_i = []
    for q,w in zip(QList,WList):
        qm = tf.expand_dims(q,-1)
        product = tf.multiply(qm,x_sep)
        p_sum = Partition_sum(product,flatAdj_0,flatAdj.values)
        
        resultProduct = tf.matmul(p_sum,w)
        y_i.append(resultProduct)
    
    _y_out = tf.add(tf.add_n(y_i),tf.reshape(var_b,(1,-1)))
    
#############################################

#    



    
    if sizes != None:
#    if False:
        MaskIndices = tf.reshape(mask_indices,(-1,2))
        y_out = tf.scatter_nd(mask_indices,_y_out,outShape)
    
    else:
        y_out = tf.reshape(_y_out,outShape)
    
    LL = tf.expand_dims(L,axis=2)
    
    loss = tf.losses.softmax_cross_entropy(LL,y_out, weights=1.0)
    optimizer = tf.train.AdamOptimizer(learning_rate=.001).minimize(loss)
    
###################################Session Definition#############################
#sess = tf.InteractiveSession()
#sess.run(tf.global_variables_initializer())
#print(y_out.eval())

sess = tf.Session(config=config)
sess.run(tf.global_variables_initializer())
fd = {V0:Vertices,adj_sp:(Adj.coords.T,Adj.data,Adj.shape),L:Labels}

_,Q = sess.run([optimizer,loss],feed_dict=fd)



"""
#Data = tf.compat.v1.convert_to_tensor_or_sparse_tensor(value=adj_sp)
#
#
#
#
#
#data_shape = tf.shape(input=Data)
#Data = tf.sparse.reshape(Data, [-1, data_shape[-2], data_shape[-1]])
#indices = Data.indices
#if True:
#  data_shape = tf.shape(input=Data, out_type=tf.int64)
#  index_shift = tf.expand_dims(indices[:, 0], -1) * data_shape[1:]
#  indices = indices[:, 1:] + index_shift
#  block_diag = tf.SparseTensor(indices, Data.values,data_shape[0] * data_shape[1:])

"""

